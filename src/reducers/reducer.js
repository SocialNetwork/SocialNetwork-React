import * as T from '../constants/ActionTypes'

export default function reducer(state, action) {
  let newState = JSON.parse(JSON.stringify(state))
  console.log("reducer called", action)
  switch (action.type) {
    case T.APP_IN_LOADING_STATE:
      newState.loading++
      console.log(`loading ${newState.loading} items`)
      return newState
    case T.DATA_RECEIVED:
      newState.loading--
      console.log(`data received, ${newState.loading} to go`)
      switch (action.from) {
        case "hello":
          newState.hello = action.payload
          break;
        case T.IMAGE_DROPPED:
          newState.fileStatus = "Processing"
          break;
        case T.IMAGE_UPLOADED:
          newState.fileStatus = "Done!"
          newState.files.shift()
          break;
        case T.USER_LIST:
          newState.usersMap = action.payload
          break;
        case T.USER_PHOTOS:
          newState.photos = action.payload.data
          break;
        case T.LIKE_COUNT:
          newState.likes[action.payload.uuid] = action.payload.count
          break;
        case T.TOGGLE_LIKE:
          newState.likes[action.payload.uuid] += (action.payload.like) ? 1 : -1
          break;
        case T.IMAGE_META_UPDATE:
          newState.updatedMeta[action.payload.uuid] = action.payload
          break;
      }
      return newState
    case T.ERROR_RECEIVED:
      newState.loading--
      console.log("error fetching:", action.from, action.error, action.retries)
      return newState
    case T.IMAGE_DROPPED:
      newState.files = action.payload
      return newState
    case T.IMAGE_UPLOAD_PROGRESS:
      const percentCompleted = Math.round((action.payload.loaded * 100) / action.payload.total)
      newState.fileStatus = percentCompleted === 100 ? 'Upload complete!' : `Upload progress: ${percentCompleted}%`
      return newState

    default:
      return state
  }
}
