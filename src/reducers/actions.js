import * as T from '../constants/ActionTypes'
import {fetchJSONAsUser} from "../auth";
import axios from 'axios'

const EMPTY_POST = {
  method: "POST",
  body: JSON.stringify({})
}

export const loading = (from) => ({type: T.APP_IN_LOADING_STATE, from})
export const fetchedData = (from, res, retries) => ({type: T.DATA_RECEIVED, from, payload: res})
export const fetchedError = (from, error, retries) => ({
  type: T.ERROR_RECEIVED, from, error, retries
})
export const fetchHello = () => (function (dispatch) {
  fetchData(dispatch, "hello", "echo")
})
export const fetchUserlist = () => (function (dispatch) {
  fetchData(dispatch, T.USER_LIST, "user")
})
export const fetchUserPhotos = (uuid) => (function (dispatch) {
  fetchData(dispatch, T.USER_PHOTOS, `photos/${uuid}`)
})
export const fetchLikes = (uuid) => (function (dispatch) {
  fetchData(dispatch, T.LIKE_COUNT, `likes/${uuid}`)
})
export const fetchPhotoMeta = (uuid) => (function (dispatch) {
  fetchData(dispatch, T.IMAGE_META_FETCH, `likes/${uuid}`)
})
export const updatePhotoMeta = (uuid, name, description, owner_id) => (function (dispatch) {
  fetchData(dispatch, T.IMAGE_META_UPDATE, `photo/${uuid}`, {
    method: "POST",
    body: JSON.stringify({
      name,
      description
    })
  })
})
export const toggleLike = (uuid) => (function (dispatch) {
  fetchData(dispatch, T.TOGGLE_LIKE, `like/${uuid}`, EMPTY_POST)
})
export const uploadProgress = (progressEvent) => ({type: T.IMAGE_UPLOAD_PROGRESS, payload: progressEvent})
export const imageDropped = (files) => (function (dispatch) {
  dispatch({type: T.IMAGE_DROPPED, payload: files})
  // file = file[0]
  files.forEach((file) => {
    console.log(file)
    fetchData(dispatch, T.IMAGE_DROPPED, `photo/new/${file.name}`)
      .then(({url:s3url, uuid}) => {
        console.log("fetchdata", uuid, s3url)
        const config = {
          headers: {
            'Content-Type': file.type
          },
          onUploadProgress: uploadProgress
        }
        dispatch(loading())
        axios.put(s3url, file, config)
          .then((data) => {
            fetchData(dispatch, T.IMAGE_META_UPDATE, `photo/${uuid}`, EMPTY_POST)
            return dispatch(fetchedData(T.IMAGE_UPLOADED, data));
          })
          .catch((err) => dispatch(fetchedError(T.IMAGE_UPLOADED, err)))
      })
  })
})

function delay(t) {
  return new Promise(function (resolve) {
    setTimeout(resolve, t)
  });
}
function fetchData(dispatch, from, path, payload, retries = 0) {
  if (retries == 0)
    dispatch(loading(from))
  return fetchJSONAsUser(`https://n2jl1g46a9.execute-api.eu-central-1.amazonaws.com/production/${path}`, payload)
    .then((data) => {
      dispatch(fetchedData(from, data));
      return data
    })
    .catch((err) => {
      console.log("error fetching data", err, retries)
      dispatch(fetchedError(from, err, retries));
      if (retries <= 3)
        return delay(250)
          .then(fetchData(dispatch, from, path, payload, retries++))
    })
}