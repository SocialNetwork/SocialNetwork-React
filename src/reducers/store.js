import reducer from "./reducer"
import {createStore, applyMiddleware} from "redux"
import thunkMiddleware from "redux-thunk"

let INITAL_STATE = {
  hello: {
    message: "Loading..."
  },
  loading: 0,
  likes: {},
  comments: {},
  updatedMeta: {}
  // files: []
};

let middleware = applyMiddleware(thunkMiddleware)

export let store = createStore(reducer, INITAL_STATE, middleware);