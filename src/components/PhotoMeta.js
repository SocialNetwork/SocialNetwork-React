import React, {Component} from 'react';
import {connectProfile, getProfile} from '../auth';
import './PhotoMeta.css';
import {Link} from 'react-router';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import * as Actions from '../reducers/actions';

class Photolist extends Component {
  static propTypes = {
    ...connectProfile.PropTypes
  };

  state = {
    editMode: false,
    name: "",
    description: ""
  }


  componentWillMount() {
    // this.props.actions.fetchLikes(this.props.src)
  }

  toggleEdit() {
    if (this.props.ownerMatch) {
      if (this.state.editMode) {
        console.log("handleSave", this.props.meta.uuid, this.name.value, this.description.value, this.props.meta.owner_id)
        this.props.actions.updatePhotoMeta(this.props.meta.uuid, this.name.value, this.description.value, this.props.meta.owner_id)
      }
      this.setState({editMode: !this.state.editMode}
      )
    }
  }

  renderEdit() {
    console.log("profile", this.props.ownerMatch)
    return (
      <div className="PhotoMeta" onDoubleClick={this.toggleEdit.bind(this)}>
        <input type="text" ref={(input) => this.name = input} defaultValue={this.props.meta.name}/>
        <p>
          <textarea ref={(input) => this.description = input} defaultValue={this.props.meta.description}/>
        </p>
      </div>
    )
  }

  renderBasic() {
    console.log("profile", this.props.ownerMatch)
    return (
      <div className="PhotoMeta" onDoubleClick={this.toggleEdit.bind(this)}>
        <h3>{this.props.meta.name}</h3>
        <p>
          {this.props.meta.description}
        </p>
      </div>
    )
  }


  render() {
    // const sub = this.props.profile.sub
    // const owner = this.props.meta.owner_id
    // const matches = (sub==owner)

    return (this.state.editMode) ? this.renderEdit() : this.renderBasic()
  }
}

const mapStateToProps = (state, props) => ({
  ownerMatch: (getProfile().sub == props.photo.owner_id),
  meta: state.updatedMeta[props.photo.uuid] || props.photo
})

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Actions, dispatch)
})

export default connect(
  mapStateToProps,
  // state => state,
  mapDispatchToProps
)(Photolist);
