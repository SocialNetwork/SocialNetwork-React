import React, {Component} from 'react';
import {connectProfile} from '../auth';
import './Users.css';
import {Link} from 'react-router';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import * as Actions from '../reducers/actions';

class Users extends Component {
  static propTypes = {
    ...connectProfile.PropTypes
  };


  componentWillMount() {
    this.props.actions.fetchUserlist()
  }

  renderUserlist() {
    if (this.props.users) {
      let render = []
      this.props.users.forEach(({user_id, nickname, picture, provider}, key) => {
        render.push(<li key={key}><Link className="Link" to={`/user/${user_id}`}><img src={picture}/>
          {nickname}
          <small>({provider})</small>
        </Link></li>)
      })
      return <ul>{render}</ul>
    } else if (this.props.children)
      return this.props.children
    else
      return <li>Loading...</li>
  }

  render() {

    return (
      <div className="Users">
        <div className="Users-heading">Users</div>
        {this.renderUserlist()}
      </div>
    );
  }
}


const mapStateToProps = state => ({
  users: new Map(state.usersMap)
})

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Actions, dispatch)
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Users);
