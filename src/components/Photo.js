import React, {Component} from 'react';
import {connectProfile} from '../auth';
import './Photo.css';
import {Link} from 'react-router';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import * as Actions from '../reducers/actions';
import PhotoMeta from './PhotoMeta'

class Photolist extends Component {
  static propTypes = {
    ...connectProfile.PropTypes
  };


  componentDidMount() {
    this.props.actions.fetchLikes(this.props.src)
  }


  render() {
    const photo = this.props.photo
    const uuid = this.props.src
    // const user = users.get(uuid) || {}

    return (
      <div className="Photo">
        <PhotoMeta photo={photo}/>
        <img src={`/photos/${uuid}`}/>
        <span className="like" onClick={() => this.props.actions.toggleLike(uuid)}>{this.props.likes} likes</span>
        {/*<span className="commentCount">{this.props.comments} Comments</span>*/}
      </div>
    );
  }
}

const mapStateToProps = (state, props) => ({
  users: new Map(state.usersMap),
  comments: state.comments[props.src],
  likes: state.likes[props.src]
})

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Actions, dispatch)
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Photolist);
