import React, {Component} from 'react';
import {connectProfile} from '../auth';
import {Link} from 'react-router';
import './Home.css';
import Dropzone from 'react-dropzone';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import * as Actions from '../reducers/actions';

class Home extends Component {
  static propTypes = {
    ...connectProfile.PropTypes
  };

  render() {

    return (
      <div className="Home">
        <div className="Home-intro">
          <h2>Welcome to our social network!</h2>
          <p>Explore your <Link to="/profile/edit">profile</Link>, check out the <Link to="/users">list of
            users</Link>.</p>
          <Dropzone className="Dropzone" activeClassName="DropzoneActive" rejectClassName="DropzoneReject"
                    ref="dropzone" onDrop={this.props.actions.imageDropped} accept="image/*">
            {({isDragActive, isDragReject}) => {
              if (isDragActive) {
                return "This file is authorized";
              }
              if (isDragReject) {
                return "This file is not authorized";
              }
              return this.props.files ? <div>
                  <h3>{this.props.fileStatus || "Authorizing"}...</h3>
                  <div>{this.props.files.map((file, i) => <img key={i} className="preview" src={file.preview}
                                                               role="presentation"/>)}</div>
                </div> : "Or upload an image";
              // {this.props.files.length} files
            }}

          </Dropzone>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Actions, dispatch)
})

export default connect(
  state => state,
  mapDispatchToProps
)(connectProfile(Home));