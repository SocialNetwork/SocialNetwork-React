import React, {PropTypes, Component} from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import * as Actions from '../reducers/actions'
import {Link} from 'react-router';
import {connectProfile, logout} from '../auth';
import logo from '../logo.svg';
import './App.css';

class App extends Component {
  static propTypes = {
    ...connectProfile.PropTypes,
    children: PropTypes.any
  };

  componentWillMount() {
    this.props.actions.fetchHello()
  }

  render() {
    return (
      <div className="Site">
        <div className="Site-header">
          <Link to="/"><img src={logo} className={`Site-logo ${(this.props.loading) ? 'loading' : ''}`}
                            alt="logo"/></Link>
          <h2>Jonas' Social Network</h2>
          {this.renderUserControls()}
        </div>
        <div className="Site-page">
          {this.props.children}
        </div>
      </div>
    )
  }

  renderUserControls() {
    const {profile} = this.props;

    if (profile) {
      return (
        <div className="Site-profileControls">
          <Link to="/profile"><img className="Site-profilePicture" src={profile.picture} alt={profile.nickname}/></Link>
          <p><Link to="/profile/edit">{profile.nickname}</Link> &middot; <a onClick={() => logout()}>Log Out</a></p>
        </div>
      );
    } else {
      return (
        <div className="Site-profileControls">
          <p><span>Guest</span> &middot; <Link to="/login">Log In</Link></p>
        </div>
      );
    }
  }
}


// App.propTypes = {
//   hello: PropTypes.object.isRequired,
//   actions: PropTypes.object.isRequired
// }

// const mapStateToProps = state => {
//   return {
//     hello: state.hello
//   }
// }

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Actions, dispatch)
})

export default connect(
  // mapStateToProps,
  state => state,
  mapDispatchToProps
)(connectProfile(App))
