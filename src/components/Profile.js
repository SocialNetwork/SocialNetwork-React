import React, {Component} from 'react';
import {connectProfile} from '../auth';
import {connect} from 'react-redux'
import {bindActionCreators} from "redux";
import * as Actions from '../reducers/actions'
import './Profile.css';
import Photolist from './Photolist'
// import AuthService from '../utils/AuthService'

// const auth = new AuthService("JBk5E6LbwkGCLpLxQBw7oHGKZ5AaILwy", "jonasd.eu.auth0.com");

class Profile extends Component {
  static propTypes = {
    ...connectProfile.PropTypes,
  };

  state = {
    error: null,
    saved: false,
    saving: false,
  }


  componentDidMount() {
  }

  render() {
    // const {profile} = this.props;
    console.log("profile", this.props)
    return (
      <div className="Profile">
        <div className="Profile-heading">Your Profile</div>
        <div className="Profile-profile">
          <p>{(this.props.hello.message) ? "Service looks up" : "There might be an issue at AWS"}</p>
        </div>
        <Photolist params={{uuid: this.props.profile.sub}}/>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Actions, dispatch)
})

export default connect(
  state => state,
  mapDispatchToProps
)(connectProfile(Profile));