import React, {Component} from 'react';
import {connectProfile} from '../auth';
import './Photolist.css';
import {Link} from 'react-router';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import * as Actions from '../reducers/actions';
import Photo from './Photo'

class Photolist extends Component {
  static propTypes = {
    ...connectProfile.PropTypes
  };


  componentWillMount() {
    if (!this.props.users.length)
      this.props.actions.fetchUserlist()
    this.props.actions.fetchUserPhotos(this.props.params.uuid)
  }

  renderPhotolist() {
    if (this.props.photos) {
      let render = []
      this.props.photos.forEach((photo, key) => {
        render.push(<Photo key={key} src={photo.uuid} photo={photo}/>)
      })
      return <section>{render}</section>
    } else
      return <p>Loading...</p>
  }

  render() {
    const users = this.props.users
    const uuid = this.props.params.uuid
    const user = users.get(uuid) || {}

    return (
      <div className="Photolist">
        <div className="Photolist-heading">Pictures from {user.nickname}
          <small>({user.provider})</small>
          {/*{this.props.users.get(this.props.params.uuid)}*/}</div>
        {this.renderPhotolist()}
        {/*<footer>Status: {this.props.statusMessage}</footer>*/}
      </div>
    );
  }
}

const mapStateToProps = (state, props) => ({
  users: new Map(state.usersMap),//.get(props.params.uuid) || {},
  photos: state.photos
})

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Actions, dispatch)
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Photolist);
