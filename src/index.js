import React from 'react'
import {render} from 'react-dom'
import {Provider} from 'react-redux'
import {Router, browserHistory} from 'react-router';
// import App from './containers/AppContainer';
import './index.css';
import {store} from "./reducers/store"
import createRoutes from "./routes";


render(
  <Provider store={store}>
    <Router history={browserHistory} routes={createRoutes()}/>
  </Provider>,
  document.getElementById('root')
)
