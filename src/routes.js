/**
 * Created by jonas on 06/03/2017.
 */
import React from 'react'
import {Route, IndexRoute} from 'react-router'

// import { SiteContainer } from './containers'
// import { Home, Login, Profile, EditProfile } from './components'
import {requireAuth} from "./auth";
import AppContainer from "./components/App";
import Home from "./components/Home";
import Login from "./components/Login";
import Profile from "./components/Profile";
import EditProfile from "./components/EditProfile";
import Users from "./components/Users";
import Photolist from "./components/Photolist";

export default function createRoutes() {
  return (
    <Route component={AppContainer}>
      <IndexRoute component={Home}/>
      <Route path="/" component={Home}/>
      <Route path="/login" component={Login}/>
      <Route path="/profile" component={Profile}/>
      <Route onEnter={requireAuth}>
        <Route path="/profile/edit" component={EditProfile}/>
        <Route path="/users" component={Users}/>
        <Route path="/user/:uuid" component={Photolist}/>
      </Route>
    </Route>
  )
}
